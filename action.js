function ready() {
    var changeable = $(".number, .slider");
    for (i = 0; i < changeable.length; i++) {
        var e = changeable[i];
        e.min = "0";
        e.max = "255";
        e.value = "63";
    }
    toCanvas();
}

function sliderChange(slider) {
    var myId = slider.id;
    var myColor = myId.substring("slider".length, myId.length);
    var targetId = "text" + myColor;
    var value = slider.value;
    var target = $("#" + targetId)[0];
    target.value = value;
    toCanvas();
}

function textChange(number) {
    var myId = number.id;
    var myColor = myId.substring("text".length, myId.length);
    var targetId = "slider" + myColor;
    var value = number.value;
    var target = $("#" + targetId)[0];
    target.value = value; 
    toCanvas();
}

function toCanvas() {
    var canvas = $("#theCanvas")[0];
    var cxt = canvas.getContext("2d");

    var r = $("#sliderRed")[0].value;
    var g = $("#sliderGreen")[0].value;
    var b = $("#sliderBlue")[0].value;

    cxt.beginPath();
    var rgb = "rgb(" + r + ", " + g + ", " + b + ")";
    cxt.fillStyle = rgb;
    cxt.fillRect(0, 0, canvas.width, canvas.height);
}

function decToHex(dec) {
    dec = (dec & 0xff);
    var hex = "";
}

function digit(at) {

}

$(document).ready(ready);
